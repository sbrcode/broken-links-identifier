import argparse
from requests import get, exceptions
from urllib.parse import urljoin
from bs4 import BeautifulSoup

def brokenlinks(url, depth):
    ''' recursive function: search for broken links and return them with error code '''
    try:
        if depth >= 1:
            htmlpage = get(url)
            soup = BeautifulSoup(htmlpage.text, 'html.parser')
            for links in soup.find_all('a'):
                new_link = urljoin(url, links.get('href'))
                link_response = get(new_link)
                if link_response.status_code >= 400:
                    print(new_link, link_response.status_code, sep='\t')
                brokenlinks(new_link, depth - 1)
    except exceptions.ConnectionError:
        pass

if __name__ == '__main__':
    ''' Retrieving Arguments with Error and Suffix Management and launching brokenlinks function'''
    parser = argparse.ArgumentParser(description="Extract all file strings from the source folder as an argument.")
    parser.add_argument("url", help="Uniform Resource Locator")
    parser.add_argument("-d", "--depth", default=1, help="research n depth", type = int)
    args = parser.parse_args()
    brokenlinks(args.url, args.depth)        