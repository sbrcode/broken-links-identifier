# brklnk
Brklnk Python3 command that browses all the links contained in a web page and displays the broken links

## prerequisites
python3
BeautifulSoup

## usage
brklnk.py [-h] [-d DEPTH] url

### Extract all file strings from the source folder as an argument
positional arguments:
* url                   Uniform Resource Locator

optional arguments:
* -h, --help<br>                show this help message and exit
* -d DEPTH, --depth DEPTH<br>   research n depth